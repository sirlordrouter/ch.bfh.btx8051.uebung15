package ch.bfh.gnagj1.util;
/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 * <p><b>Immutable Class</b></p>
 *<p>A Class whose sense is not clear yet....</p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 25.09.2012
 */
public class FormattedDate {
	
	public final int day;
	public final int month;
	public final int year;

	/**
	 * Constructs a FormattedDate object on the base of a defined date or the
	 * defined number of days, months and years.
	 * 
	 * @param aDay
	 *           Day of a date or a number of days (without leading 0).
	 * @param aMonth
	 *           Month of a date or an number of months (without leading 0).
	 * @param aYear
	 *           The year of a date or a number of years (without leading 0).
	 */
	public FormattedDate(int aDay, int aMonth, int aYear)
	{
		day = aDay;
		month = aMonth;
		year = aYear; 
	}
	
	/**
	 * Constructs a FormattedDate object on the base of a defined date or the
	 * defined number of days, months and years.
	 * 
	 * @param aDate
	 *           A date or a number of days, months, years in the following
	 *           format: dd.mm.yyyy
	 */
	public FormattedDate(String aDate)
	{
		day = Integer.parseInt(aDate.substring(0, 2));
		month = Integer.parseInt(aDate.substring(3, 5));
		year = Integer.parseInt(aDate.substring(6, 10));
	}
}
