package ch.bfh.gnagj1.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

import ch.bfh.gnagj1.Base.Person;
import ch.bfh.gnagj1.uebung12.Address;
import ch.bfh.gnagj1.uebung12.Doctor;
import ch.bfh.gnagj1.uebung12.Patient;

/**
 * Berner Fachhochschule</br> Medizininformatik BSc</br> Modul 8051-HS2012</br>
 * 
 * <p>
 * Reads Persondata from a given Textfile and creates correspondig
 * <b>Patient</b> and <b>Doctor</b> Objects.
 * </p>
 * 
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 17.12.2012
 */
public class PersonDataIO {

	/**
	 * Reads personData from a File where each Persondata-Block uses 80 (Docotr)
	 * or 120 (Patient) signs. The inputFile must be according to Uebung 15's
	 * file.
	 * 
	 * @param aFilePath
	 *            path to the File where all the Persondata ist stored in.
	 * @return ArrayList an List of all Persons parsed from the File given.
	 */
	public static ArrayList<Person> readPersonData(String aFilePath) {

		ArrayList<Person> persons = new ArrayList<Person>();
		File inputFile = new File(aFilePath);
		Scanner scanner;

		try {
			scanner = new Scanner(inputFile);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();

				String[] data = trimData(line);

				if (line.length() == 120) {

					String[] date = data[2].split("-");
					Patient aPatient = new Patient(data[0], data[1],
							new GregorianCalendar(Integer.parseInt(date[0]),
									Integer.parseInt(date[1]),
									Integer.parseInt(date[2])), new Address(
									data[3], data[4], data[5]));
					persons.add(aPatient);
				} else if (line.length() == 80) {
					Doctor aDoctor = new Doctor(data[0], data[1], data[2],
							data[3]);
					persons.add(aDoctor);
				}

			}
			scanner.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return persons;
	}

	/**
	 * 
	 * @param aPersonList
	 *            a list with Persons, either Doctors or Patients.
	 * @param aFilePath
	 *            path where the File will be stored.
	 * @return boolean
	 *         <ul>
	 *         <li>true: the operation was successfull</li>
	 *         <li>false: the file could not be written</li>
	 *         </ul>
	 */
	public static boolean writePersonFile(ArrayList<Person> aPersonList,
			String aFilePath) {
		try {
			PrintWriter writer = new PrintWriter(aFilePath);
			String line = "";
			for (Person person : aPersonList) {

				if (person instanceof Doctor) {
					Doctor doctor = (Doctor) person;
					line = String.format("%s\t%s\t%s\t%s\t%s", "d",
							doctor.getFirstname(), doctor.getLastname(),
							doctor.getTitle(), doctor.getPhone());
				} else {
					Patient patient = (Patient) person;
					line = String.format("%s\t%s\t%s\t%s\t%s", "p",
							patient.getFirstname(), patient.getLastname(),
							patient.getBirthdate(), patient.getAddress());
				}

				writer.println(line);
			}

			writer.close();
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	private static String[] trimData(String aString) {
		int size = aString.length() / 20;
		String[] data = new String[size];

		for (int i = 20; i <= aString.length(); i += 20) {
			int j = i - 20;
			int index = (i / 20) - 1;
			data[index] = aString.substring(j, i).trim();
		}

		return data;
	}

}
