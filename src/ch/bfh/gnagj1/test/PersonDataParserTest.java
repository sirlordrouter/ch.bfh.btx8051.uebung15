package ch.bfh.gnagj1.test;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import ch.bfh.gnagj1.Base.Person;
import ch.bfh.gnagj1.util.*;

public class PersonDataParserTest {

	/**
	 * Tests if read/write Person-Data from Filesystem is successfull. 
	 * 
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub

		ArrayList<Person> persons = PersonDataIO.readPersonData("/Users/Johannes/Daten/Ausbildung/Medizininformatik Bsc/Module/8051_Pb_Einfuehrung-in-die-Programmierung/Uebungen/Personendaten.txt");
		for (Person person : persons) {
			System.out.println(person.toString()); 
		}
		
		PersonDataIO.writePersonFile(persons, "/Users/Johannes/Daten/Ausbildung/Medizininformatik Bsc/Module/8051_Pb_Einfuehrung-in-die-Programmierung/Uebungen/PersonendatenOut.txt");
	}

}
